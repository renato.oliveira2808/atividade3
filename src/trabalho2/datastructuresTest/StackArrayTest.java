package datastructuresTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import algorithms.datastructures.StackArray;

public class StackArrayTest {
	
	// Iniciando um vetor com construtor sem par�metros e vendo que est� vazio
	@DisplayName(value = "Construtor sem par�metros")
	@Test
	public void testInitialSizeEmptyConstructorNoParameters() {
		StackArray sa = new StackArray();
		boolean empty = sa.isEmpty();
		assertTrue(empty);
	}
	
	// Iniciando um vetor com construtor com par�metro e vendo que est� vazio
	@DisplayName(value = "Construtor com par�metros")
	@Test
	public void testInitialSizeConstructorWithSize() {
		StackArray sa = new StackArray(10);
		boolean empty = sa.isEmpty();
		assertTrue(empty);
	}
	
	// Inserindo um elemento e checando novamente o tamanho do vetor
	@DisplayName(value = "Primeiro Push")
	@Test
	public void testFirstPush() {
		StackArray sa = new StackArray();
		sa.push(10);
		int size = sa.size();
		assertEquals(1, size);
	}
	
	// Inserindo 10 elementos, retirando 1 e pegando o que est� no topo
	@DisplayName(value = "Peek ap�s 10 pushes e um pop")
	@Test
	public void testPushPopPeek() {
		StackArray sa = new StackArray();
		for(int i = 0; i < 10; i++) {
			sa.push(i);
		}
		sa.pop();
		int value = sa.peek();
		assertEquals(8, value);
	}
	
	// Inserindo um elemento e confirmando pela fun��o peek se ele est� no topo
	@DisplayName(value = "Push e Peek")
	@Test
	public void testPeek() {
		StackArray sa = new StackArray();
		sa.push(10);
		int value = sa.peek();
		assertEquals(10, value);
	}
	
	// Testando a fun��o peek num vetor vazio
	@DisplayName(value = "Peek em Array vazio")
	@Test
	public void testPeekEmpty() {
		StackArray sa = new StackArray();
		int value = sa.peek();
		assertEquals(-1, value);
	}
	
	// Testando a peek depois de inserir 10 elementos no vetor
	@DisplayName(value = "Peek depois de inserir 10 elementos")
	@Test
	public void testPeekAfterPushTenElements() {
		StackArray sa = new StackArray();
		for(int i = 0; i < 10; i++) {
			sa.push(i);
		}
		int value = sa.peek();
		assertEquals(9, value);
	}
	
	// Testando a peek ap�s inserir um elemento e retir�-lo
	@DisplayName(value = "Peek ap�s um push e um pop")
	@Test
	public void testPeekAfterPushAndPop() {
		StackArray sa = new StackArray();
		sa.push(10);
		sa.pop();
		int value = sa.peek();
		assertEquals(-1, value);
	}
	
	// Testando o que ocorre quando s�o inseridos 7 elementos num vetor com 5 de capacidade
	@DisplayName(value = "Teste push com overload")
	@Test
	public void testArrayOverload() {
		StackArray sa = new StackArray(5);
		for(int i = 0; i < 7; i++) {
			sa.push(i);
		}
		int value = sa.peek();
		assertEquals(6, value);
	}
	
	// Esse teste foi �til para perceber que no momento do resize a fun��o push n�o inseria o valor
	@DisplayName(value = "Teste push com overload 2")
	@Test
	public void testArrayOverloadFinishInResize() {
		StackArray sa = new StackArray(5);
		for(int i = 0; i < 6; i++) {
			sa.push(i);
		}
		int value = sa.peek();
		assertEquals(5, value);
	}
	
	// Testando a fun��o isFull
	@DisplayName(value = "Fun��o isFull")
	@Test
	public void testArrayFull() {
		StackArray sa = new StackArray(1);
		sa.push(10);
		boolean full = sa.isFull();
		assertTrue(full);
	}
	
	// Testando a fun��o makeEmpty
	@DisplayName(value = "Fun��o makeEmpty")
	@Test
	public void testMakeEmpty() {
		StackArray sa = new StackArray(10);
		for(int i = 0; i < 10; i++) {
			sa.push(i);
		}
		sa.makeEmpty();
		boolean empty = sa.isEmpty();
		assertTrue(empty);
	}
	
	// Testando a retirada de elementos de um vetor vazio
	@DisplayName(value = "Pop em array vazio")
	@Test
	public void testPopEmptyArray() {
		StackArray sa = new StackArray(1);
		int value = sa.pop();
		assertEquals(-1, value);
	}
	
	// Testando a fun��o criada capacity com o construtor vazio
	@DisplayName(value = "Fun��o criada capacity")
	@Test
	public void testCapacity() {
		StackArray sa = new StackArray();
		int value = sa.capacity();
		assertEquals(10, value);
	}
	
	// Testando a fun��o criada capacity com um construtor com par�metros
	@DisplayName(value = "Fun��o criada capacity com construtor com par�metros")
	@Test
	public void testCapacityWithParameters() {
		StackArray sa = new StackArray(8);
		int value = sa.capacity();
		assertEquals(8, value);
	}
	
	// Testando altera��o de tamanho do vetor pelo fun��o push
	@DisplayName(value = "Altera��o de tamanho com o push")
	@Test
	public void testResizeInPush() {
		StackArray sa = new StackArray(5);
		for(int i = 0; i < 6; i++) {
			sa.push(i);
		}
		int value = sa.capacity();
		assertEquals(value, 10);
	}
	
}
